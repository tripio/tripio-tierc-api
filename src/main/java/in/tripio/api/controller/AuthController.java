/**
 * 
 */
package in.tripio.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.tripio.api.httpResponse.AuthResponse;
import in.tripio.api.model.Usrp;
import in.tripio.api.service.AuthService;
import in.tripio.api.util.JwtUtil;

/**
 * @author Anant
 *
 */
@RestController
@RequestMapping(value = "in/tripio/api")
public class AuthController {

	private AuthenticationManager authManager;
	private JwtUtil jwtUtil;
	private AuthService authService;

	public AuthController(AuthenticationManager authManager, JwtUtil jwtUtil, AuthService authService) {
		this.authManager = authManager;
		this.jwtUtil = jwtUtil;
		this.authService = authService;
	}

	@PostMapping("/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody Usrp usrp) throws Exception {

		try {
			authManager.authenticate(
					new UsernamePasswordAuthenticationToken(usrp.getUsrp_Email(), usrp.getUsrp_Password()));
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}

		final UserDetails userDetails = authService.loadUserByUsername(usrp.getUsrp_Email());

		final String token = jwtUtil.generateToken(userDetails);

		return ResponseEntity.ok(new AuthResponse(token));
	}
}
