/**
 * 
 */
package in.tripio.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.tripio.api.model.Trip;
import in.tripio.api.service.TripService;

/**
 * @author Anant
 *
 */
@RestController
@RequestMapping(value = "in/tripio/api")
public class TripController {

	@Autowired
	private TripService tripService;
	
	@GetMapping("/allTrips")
	public List<Trip> getAllTrips()
	{
		return tripService.getAllTrips();
	}
	
	@GetMapping("/trip/{tripID}")
	public Trip getTrip(@PathVariable("tripID") int trip_ID)
	{
		return tripService.getTrip(trip_ID);
	}
	
	@GetMapping("/")
	public String test() 
	{
		return "Hello";
	}
	
}
