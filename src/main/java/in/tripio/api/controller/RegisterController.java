/**
 * 
 */
package in.tripio.api.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import in.tripio.api.model.Usrd;

/**
 * @author Anant
 *
 */
@RequestMapping(value = "in/tripio/api")
public interface RegisterController {

	@PostMapping("/register")
	public Usrd createUser(@RequestBody Usrd usrd);
}
