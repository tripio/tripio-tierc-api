/**
 * 
 */
package in.tripio.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import in.tripio.api.model.Usrd;
import in.tripio.api.service.RegisterService;

/**
 * @author Anant
 *
 */
@RestController
public class RegisterControllerImpl implements RegisterController {

	@Autowired
	private RegisterService registerService;

	@Override
	public Usrd createUser(Usrd usrd) {

		return registerService.createUser(usrd);

	}

}
