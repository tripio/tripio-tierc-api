
package in.tripio.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Anant
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Trip {
	private int trip_ID;
	private String trip_Code;
	private double trip_startingPrice;
	private String trip_StartDate;
	private String trip_EndDate;
	private int trip_DepartingCity_TPOC_ID;
	private int trip_DestinationCity_TPOC_ID;
	private String trip_PlaceCovered;
}
