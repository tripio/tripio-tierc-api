/**
 * 
 */
package in.tripio.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Anant
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usrd {
	
	private int usrd_ID;
	private String usrd_Name;
	private String usrd_DOB;
	private String usrd_Gender;
	private String usrd_Nationality;
	private String usrd_Address;
	private String usrd_City;
	private String usrd_State;
	private int usrd_PinCode;
	private String usrd_Phone;
	private Usrp usrp;
}
