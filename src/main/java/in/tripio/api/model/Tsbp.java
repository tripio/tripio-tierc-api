/**
 * 
 */
package in.tripio.api.model;

import lombok.Data;

/**
 * @author Anant
 *
 *TSBP: Trip Specific Boarding Point
 */
@Data
public class Tsbp {

	private int tsbp_ID;
	private int tsbp_Trip_ID;
	private int tsbp_Tpoc_ID;
	private double tsbp_Bdpt_ID;
}
