/**
 * 
 */
package in.tripio.api.model;

import lombok.Data;

/**
 * @author Anant
 *
 *TSBC: Trip Specific Boarding City
 */
@Data
public class Tsbc {
	private int tsbc_ID;
	private int tsbc_Trip_ID;
	private int tsbc_Tpoc_ID;
	private double tsbc_Price;
}
