/**
 * 
 */
package in.tripio.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Anant
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usrp {
	
	private int usrp_USRD_ID;
	private String usrp_Email;
	private String usrp_Password;
	private String usrp_PasswordSalt;

}
