/**
 * 
 */
package in.tripio.api.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import lombok.Data;

/**
 * @author Anant
 *
 */
@Configuration
@EnableConfigurationProperties(value = { MySQLDBConfig.SqlDataSourceProps.class })
public class MySQLDBConfig {
	@ConfigurationProperties(prefix = "sql.database")
	@Data
	public class SqlDataSourceProps {
		private String driverClass;
		private String jdbcUrl;
		private String userName;
		private String password;
		
	}
	
	@Autowired
	private SqlDataSourceProps SQLDb;
	
	@Bean
	public NamedParameterJdbcTemplate oracleNamedTemplate() {
		return new NamedParameterJdbcTemplate(getBasicDatasource());
	}
	
	@Bean
	public DataSource getBasicDatasource() {

		BasicDataSource datasource = new BasicDataSource();
		datasource.setUrl(SQLDb.getJdbcUrl());
		datasource.setDriverClassName(SQLDb.getDriverClass());
		datasource.setUsername(SQLDb.getUserName());
		datasource.setPassword(SQLDb.getPassword());

		return datasource;
	}
}
