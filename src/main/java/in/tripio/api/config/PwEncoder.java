/**
 * 
 */
package in.tripio.api.config;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Anant
 *
 */
public class PwEncoder implements PasswordEncoder {

	// later value for pepper
	private final String pepper = "fah8429jdn@#%&MHLK46\6439";
	private final String algoType = "HmacSHA256";
	private final int stregth = 10;

	@Override
	public String encode(CharSequence rawPassword) {

		return BCrypt.hashpw(encodeHmac(rawPassword.toString()), BCrypt.gensalt(stregth));
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {

		return BCrypt.checkpw(encodeHmac(rawPassword.toString()), encodedPassword);
	}

	private byte[] encodeHmac(String rawPassword) {
		Mac mac;
		byte[] hashedPw = null;
		try {
			mac = Mac.getInstance(algoType);
			SecretKeySpec secretKeySpec = new SecretKeySpec(pepper.getBytes("UTF-8"), algoType);
			mac.init(secretKeySpec);
			hashedPw = mac.doFinal(rawPassword.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("Pepper used: "+pepper);
		System.out.println("hmac hased pass: "+hashedPw);
		return hashedPw;
	}
	
//	public static void main(String argd[]) {
//		PwEncoder pe = new PwEncoder();
	//"usrp_Email": "a@g.com",
    //"usrp_Password": "Xyz@12345"
//		System.out.println(pe.encode("Xyz@123"));
//	}
}
