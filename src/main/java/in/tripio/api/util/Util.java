/**
 * 
 */
package in.tripio.api.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

/**
 * @author Anant
 *
 */
@Component
public class Util {

	private final String SEQN_SQL = "SELECT SEQN_LASTID FROM SEQN WHERE SEQN_TABLENAME =:tableName";
	private final String UPDATE_SEQN_LASTID = "UPDATE SEQN SET SEQN_LASTID = :lastID WHERE SEQN_TABLENAME =:tableName";

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	public int getNextSEQN(String tableName) {

		Map<String, Object> param_SEQN_SQL = new HashMap<String, Object>();
		param_SEQN_SQL.put("tableName", tableName);
		System.out.println("Query: " + SEQN_SQL);
		SqlRowSet rs = jdbcTemplate.queryForRowSet(SEQN_SQL, param_SEQN_SQL);
		if (rs.next()) {
			int lastID = rs.getInt(1) + 1;
			Map<String, Object> param_UPDATE_SEQN_LASTID = new HashMap<String, Object>();
			param_UPDATE_SEQN_LASTID.put("lastID", lastID);
			param_UPDATE_SEQN_LASTID.put("tableName", tableName);
			jdbcTemplate.update(UPDATE_SEQN_LASTID, param_UPDATE_SEQN_LASTID);
			System.out.println("Fectched SEQ Number for "+tableName+" : "+lastID);
			return lastID;
		}
		throw new RuntimeException();
	}

}
