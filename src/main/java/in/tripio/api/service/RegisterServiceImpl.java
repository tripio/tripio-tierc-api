/**
 * 
 */
package in.tripio.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import in.tripio.api.model.Usrd;
import in.tripio.api.repository.RegisterRepository;

/**
 * @author Anant
 *
 */
@Service
public class RegisterServiceImpl implements RegisterService {

	@Autowired
	RegisterRepository registerRepository;
	@Autowired
	PasswordEncoder pwEncoder;

	@Override
	public Usrd createUser(Usrd usrd) {

		usrd.getUsrp().setUsrp_Password(pwEncoder.encode(usrd.getUsrp().getUsrp_Password()));
		return registerRepository.createUser(usrd);
	}


}
