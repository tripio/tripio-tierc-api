/**
 * 
 */
package in.tripio.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.tripio.api.model.Trip;
import in.tripio.api.repository.TripRepository;

/**
 * @author Anant
 *
 */
@Service
public class TripServiceImpl implements TripService{

	@Autowired
	private TripRepository tripReository;
	
	@Override
	public List<Trip> getAllTrips() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Trip getTrip(int trip_ID) {
		return tripReository.getTrip(trip_ID);
	}

}
