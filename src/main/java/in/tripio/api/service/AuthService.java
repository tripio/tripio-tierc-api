/**
 * 
 */
package in.tripio.api.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import in.tripio.api.model.Usrp;
import in.tripio.api.repository.RegisterRepository;

/**
 * @author Anant
 *
 */
@Service
public class AuthService implements UserDetailsService {

	@Autowired
	RegisterRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usrp usrp = userRepo.getUserByUserName(username);
		System.out.println(usrp);
		if (usrp != null) {
			return new User(usrp.getUsrp_Email(), usrp.getUsrp_Password(), new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

}
