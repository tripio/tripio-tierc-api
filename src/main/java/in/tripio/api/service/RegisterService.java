/**
 * 
 */
package in.tripio.api.service;

import in.tripio.api.model.Usrd;

/**
 * @author Anant
 *
 */
public interface RegisterService {

	public Usrd createUser(Usrd usrd);
}
