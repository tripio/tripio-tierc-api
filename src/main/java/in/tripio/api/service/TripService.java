/**
 * 
 */
package in.tripio.api.service;

import java.util.List;

import in.tripio.api.model.Trip;

/**
 * @author Anant
 *
 */
public interface TripService {

	public List<Trip> getAllTrips();
	
	public Trip getTrip(int trip_ID);
}
