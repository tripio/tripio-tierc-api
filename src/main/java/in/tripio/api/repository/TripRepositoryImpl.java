package in.tripio.api.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import in.tripio.api.model.Trip;
import in.tripio.api.repository.rowmapper.TripRowMapper;

@Repository
public class TripRepositoryImpl implements TripRepository{

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public Trip getTrip(int tripID){
		String query = "Select * from TRIP where TRIP_ID=:id";
		final MapSqlParameterSource param= new MapSqlParameterSource().addValue("id", 1);
		
		return jdbcTemplate.queryForObject(query, param, new TripRowMapper());
	}
	
	
}
