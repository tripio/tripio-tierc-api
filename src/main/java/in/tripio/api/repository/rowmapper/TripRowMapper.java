/**
 * 
 */
package in.tripio.api.repository.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import in.tripio.api.model.Trip;

/**
 * @author Anant
 *
 */
public class TripRowMapper implements RowMapper<Trip>{

	@Override
	public Trip mapRow(ResultSet rs, int rowNum) throws SQLException {
		Trip trip = new Trip();
		trip.setTrip_ID(rs.getInt("TRIP_ID"));
		trip.setTrip_Code(rs.getString("TRIP_CODE"));
		trip.setTrip_startingPrice(rs.getDouble("TRIP_StartingPrice"));
		trip.setTrip_StartDate(rs.getString("TRIP_StartDate"));
		trip.setTrip_EndDate(rs.getString("TRIP_EndDate"));
		trip.setTrip_DepartingCity_TPOC_ID(rs.getInt("Trip_DepartingCity_TPOC_ID"));
		trip.setTrip_DestinationCity_TPOC_ID(rs.getInt("Trip_DestinationCity_TPOC_ID"));
		trip.setTrip_PlaceCovered(rs.getNString("TRIP_PLACECOVERED"));
		return trip;
	}
}
