/**
 * 
 */
package in.tripio.api.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import in.tripio.api.model.Usrd;
import in.tripio.api.model.Usrp;
import in.tripio.api.util.Util;

/**
 * @author Anant
 *
 */
@Repository
public class RegisterRepositoryImpl implements RegisterRepository {

	private final String INSERT_USRD = "INSERT INTO USRD(USRD_ID, USRD_NAME, USRD_DOB, USRD_GENDER, USRD_NATIONALITY, USRD_ADDRESS, USRD_CITY, USRD_STATE, USRD_PINCODE, USRD_PHONE) "
			+ "values(:id,:name,:dob,:gender,:nationality,:address,:city,:state,:pinCode,:phone)";
	private final String INSERT_USRP = "INSERT INTO USRP(USRP_USRD_ID, USRP_EMAIL, USRP_PASSWORD) "
			+ "values(:id,:email,:password)";
	private final String FETCH_USER_BY_USERNAME = "SELECT USRP_USRD_ID, USRP_EMAIL, USRP_PASSWORD from USRP where USRP_EMAIL = :userName";

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	Util util;

	@Override
	public Usrd createUser(Usrd usrd) {

		Map<String, Object> param_INSERT_USRD = new HashMap<String, Object>();
		param_INSERT_USRD.put("id", util.getNextSEQN("USRD"));
		param_INSERT_USRD.put("name", usrd.getUsrd_Name());
		param_INSERT_USRD.put("dob", usrd.getUsrd_DOB());
		param_INSERT_USRD.put("gender", usrd.getUsrd_Gender());
		param_INSERT_USRD.put("nationality", usrd.getUsrd_Nationality());
		param_INSERT_USRD.put("address", usrd.getUsrd_Address());
		param_INSERT_USRD.put("city", usrd.getUsrd_City());
		param_INSERT_USRD.put("state", usrd.getUsrd_State());
		param_INSERT_USRD.put("pinCode", usrd.getUsrd_PinCode());
		param_INSERT_USRD.put("phone", usrd.getUsrd_Phone());
		jdbcTemplate.update(INSERT_USRD, param_INSERT_USRD);

		// setting parameter for insertion in USRP
		Map<String, Object> param_INSERT_USRP = new HashMap<String, Object>();
		param_INSERT_USRP.put("id", param_INSERT_USRD.get("id"));
		param_INSERT_USRP.put("email", usrd.getUsrp().getUsrp_Email());
		param_INSERT_USRP.put("password", usrd.getUsrp().getUsrp_Password());
		jdbcTemplate.update(INSERT_USRP, param_INSERT_USRP);

		return usrd;
	}

	@Override
	public Usrp getUserByUserName(String userName) {
		Map<String, Object> param_FETCH_USER_BY_USERNAME = new HashMap<String, Object>();
		param_FETCH_USER_BY_USERNAME.put("userName", userName);

		return jdbcTemplate.queryForObject(FETCH_USER_BY_USERNAME, param_FETCH_USER_BY_USERNAME,
				(rs, rowNum) -> new Usrp(rs.getInt(1), rs.getString(2), rs.getString(3), ""));

	}

}
