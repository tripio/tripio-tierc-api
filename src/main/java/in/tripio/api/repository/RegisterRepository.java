/**
 * 
 */
package in.tripio.api.repository;

import in.tripio.api.model.Usrd;
import in.tripio.api.model.Usrp;

/**
 * @author Anant
 *
 */
public interface RegisterRepository {

	public Usrd createUser(Usrd usrd);

	Usrp getUserByUserName(String userName);
}
