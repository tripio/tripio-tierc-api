/**
 * 
 */
package in.tripio.api.repository;

import in.tripio.api.model.Trip;

/**
 * @author Anant
 *
 */
public interface TripRepository {

	//public List<Trip> getAllTrips();
	public Trip getTrip(int trip_ID);
}
